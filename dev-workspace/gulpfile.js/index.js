const gulp = require("gulp"),
  HTML = require("./html"),
  Server = require("./server"),
  JS = require("./javascript");

let html = new HTML(),
  server = new Server(),
  js = new JS();

let start = gulp.series(
  gulp.parallel(html._copyHTML, js._build),
  server._start
);

exports.default = start;
