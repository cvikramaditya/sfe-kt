const { exec } = require("child_process");
class JS {
  constructor() {}
  _build(cb) {
    exec(
      "parcel build ./app/js/*.js --out-dir ./dist/js --public-url /js/",
      (error, stdout, stderr) => {
        if (error) {
          console.log(`error: ${error.message}`);
          return;
        }
        if (stderr) {
          console.log(`stderr: ${stderr}`);
          return;
        }
        console.log(`stdout: ${stdout}`);
        cb();
      }
    );
  }
}

module.exports = JS;
