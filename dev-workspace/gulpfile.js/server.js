const browserSync = require("browser-sync").create(),
  gulp = require("gulp");

class Server {
  constructor() {}
  _start() {
    return browserSync.init({
      server: {
        baseDir: "./dist",
      },
    });
  }
}

module.exports = Server;
