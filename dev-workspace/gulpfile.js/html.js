const gulp = require("gulp");

class HTML {
  constructor() {}
  _copyHTML() {
    return gulp.src("app/html/**/*.html").pipe(gulp.dest("dist"));
  }
}

module.exports = HTML;
